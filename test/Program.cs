﻿using System;
using System.IO;
using System.Text;

namespace test {
    public static class Program {
        static void Main(string[] args) {
            int choice;
            // getting how to enter inpur string
            while (true) {
                Console.WriteLine("Choose how to enter data: 0 - file , 1 - console");
                choice = int.Parse(Console.ReadLine());
                if (choice == 1 || choice == 0)
                    break;
                else
                    Console.WriteLine("Try again");
            }

            // handling choice
            string input = choice switch {
                0 => GetStringFromFile(),
                _ => Console.ReadLine(),
            };

            // getting an array of strings with nums without other symbols
            string[] coordinates = input.Split(new char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);

            // handling empty input from console / file
            if (coordinates.Length == 0) {
                Console.WriteLine("Empty input");
                return;
            }

            // handling wrong input
            if (coordinates.Length % 2 == 1) {
                Console.WriteLine("Wrong input(odd amount of coordinates)");
                return;
            }

            // showing the result
            for (int i = 0; i < coordinates.Length; i++)
                Console.Write($"{(i % 2 == 0 ? "X" : "Y")} : {coordinates[i],-10} {(i % 2 == 1 ? "\n" : "")}");

        }

        public static string GetStringFromFile() {
            FileStream inputFile = File.Open("input.txt", FileMode.OpenOrCreate);
            if (inputFile.Length == 0)
                return string.Empty;
            byte[] array = new byte[200]; // maximum input
            inputFile.Read(array);
            inputFile.Close();
            return System.Text.Encoding.Default.GetString(array);
        }
    }
}
